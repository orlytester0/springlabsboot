<%-- <%@page import="java.util.Date"%> --%>
<%@page import="java.util.Date"%>
<%@ include file="/WEB-INF/layouts/include.jsp"%>
<h1 class="center">Kung Foo LLC</h1>
<h2 class="center">Course Report</h2>
<div class="mb-2">
	<span>
		Total Transactions: ${txCount}
	</span>
	<span class="float-right">
		<%=new Date() %>
	</span>
</div>

<c:forEach var="entry" items="${classMap}">
	<%-- Group By Value (Class Name) --%>
	<div class="oreillyGreen strong">
		${entry.key}
	</div>
	
	<%-- Course Information --%>
	<div class="ml-2">
		<c:set var="clazz" value="${entry.value}" />
		Instructor=${clazz.instructor} Days=${clazz.days} Start=${clazz.start} Duration=${clazz.duration}
	</div>

	<%-- Pupil Report --%>	
	<div class="ml-3">
		<c:forEach items="${clazz.pupilList}" var="pupil">
			${pupil}<br/>
		</c:forEach>
	</div>
</c:forEach>

<c:forEach items="${classList}" var="clazz">
	${clazz}<br/>
</c:forEach>