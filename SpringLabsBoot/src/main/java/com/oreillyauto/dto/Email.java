package com.oreillyauto.dto;

import java.io.Serializable;

public class Email implements Serializable {
	
	private static final long serialVersionUID = -6587431151280708554L;
	private String emailAddress;
	private String emailBody;
	private String messageType;
	private String message;
	
	public Email() {
	}

	public Email(String emailAddress, String emailBody) {
		this.emailAddress = emailAddress;
		this.emailBody = emailBody;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getEmailBody() {
		return emailBody;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
