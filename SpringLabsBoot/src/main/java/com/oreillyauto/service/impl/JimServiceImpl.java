package com.oreillyauto.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.JimRepository;
import com.oreillyauto.domain.Course;
import com.oreillyauto.domain.University;
import com.oreillyauto.service.JimService;

@Service("jimService")
public class JimServiceImpl implements JimService {
    
    @Autowired
    JimRepository jimRepo;

    @Override
    public Map<String, Course> getDetails() {
        return jimRepo.getDetails();
    }

    @Override
    public Map<String, List<University>> getReport() {
        return jimRepo.getReport();
    }

    
    
}
